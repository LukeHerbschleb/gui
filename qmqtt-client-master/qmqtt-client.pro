#-------------------------------------------------
#
# Project created by QtCreator 2013-03-22T21:02:13
#
#-------------------------------------------------

QT += core gui network scxml

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11
STATECHARTS += device.scxml

TARGET = qmqtt-client
TEMPLATE = app

#NOTICE: add DYLD_LIBRARY_PATH to build environment.
#INCLUDEPATH += ../src
#LIBS += -L../src -lqmqtt

INCLUDEPATH += $$PWD/../qmqtt-master/src/mqtt

message("Build directory = " $$OUT_PWD)

#LIBS += -L$$OUT_PWD/lib  -lqmqtt
LIBS += -L/home/jos/Projects/SVN/KaribaIoT/trunk-IoT/MQTTgui/build-qmqtt-client-Desktop_Qt_5_10_1_GCC_64bit-Debug/lib/ -lqmqtt
#LIBS += -L/home/jos/Projects/KaribaIoT/trunk-IoT/MQTTgui/build-qmqtt-Desktop_Qt_5_9_1_GCC_64bit-Debug/lib/ -lqmqtt

SOURCES += main.cpp \
    mainwindow.cpp \
    connform.cpp \
    pubform.cpp \
    subform.cpp \
    deviceForm.cpp
        mainwindow.cpp

HEADERS += mainwindow.h \
    connform.h \
    pubform.h \
    subform.h \
    mqttform.h \
    deviceForm.h

FORMS += mainwindow.ui \
    connform.ui \
    pubform.ui \
    subform.ui \
    deviceForm.ui

DISTFILES += \
    device.qmodel

STATECHARTS += \
    device.scxml

#include "deviceForm.h"
#include "ui_deviceForm.h"

#include <iostream>

DeviceForm::DeviceForm(QWidget *parent) :
    // ##
    // QWidget(parent),
    MqttForm(parent),
    ui(new Ui::DeviceForm),
    stateMachine{QScxmlStateMachine::fromFile("/home/jos/Projects/KaribaIoT/trunk-IoT/MQTTgui/qmqtt-client-master/device.scxml")}
{
    QVector<QScxmlError> fsmErrors = stateMachine->parseErrors();

    for(auto perr: fsmErrors)
    {
        qDebug() << perr.description();
    }

    ui->setupUi(this);
    stateMachine->start();
    if(stateMachine->isRunning())
    {
        qDebug() << "State machine is running";
    }
    stateMachine->connectToState("S_Counting", [](bool active) {
        qDebug() << (active ? " Entered" : " Exited") << "the S_Counting state";
    });
    stateMachine->connectToState("S_Resetting", [](bool active) {
        qDebug() << (active ? " Entered" : " Exited") << "the S_Resetting state";
    });
}

DeviceForm::~DeviceForm()
{
    delete ui;
}

void DeviceForm::on_buttonpub_clicked()
{
    QMQTT::Message msg_0(0, "ESEiot/1819sep/591725/Composite_Control/WebApp/temperature", ui->lineTemp->text().toUtf8());
    _client->publish(msg_0);
    QMQTT::Message msg_1(0, "ESEiot/1819sep/591725/Composite_Control/WebApp/humidity", ui->lineHumi->text().toUtf8());
    _client->publish(msg_1);
    QMQTT::Message msg_2(0, "ESEiot/1819sep/591725/Composite_Control/WebApp/pressure", ui->linePres->text().toUtf8());
    _client->publish(msg_2);
}

void DeviceForm::on_greenon_clicked()
{
    QMQTT::Message msg_3(0, "ESEiot/1819sep/591725/Composite_Control/WebApp/ledgreen", "1");
    _client->publish(msg_3);
}

void DeviceForm::on_yellowon_clicked()
{
    QMQTT::Message msg_3(0, "ESEiot/1819sep/591725/Composite_Control/WebApp/ledyellow", "1");
    _client->publish(msg_3);
}

void DeviceForm::on_redon_clicked()
{
    QMQTT::Message msg_3(0, "ESEiot/1819sep/591725/Composite_Control/WebApp/ledred", "1");
    _client->publish(msg_3);
}

void DeviceForm::on_greenoff_clicked()
{
    QMQTT::Message msg_3(0, "ESEiot/1819sep/591725/Composite_Control/WebApp/ledgreen", "0");
    _client->publish(msg_3);
}

void DeviceForm::on_yellowoff_clicked()
{
    QMQTT::Message msg_3(0, "ESEiot/1819sep/591725/Composite_Control/WebApp/ledyellow", "0");
    _client->publish(msg_3);
}

void DeviceForm::on_redoff_clicked()
{
    QMQTT::Message msg_3(0, "ESEiot/1819sep/591725/Composite_Control/WebApp/ledred", "0");
    _client->publish(msg_3);
}

#ifndef DEVICEFORM_H
#define DEVICEFORM_H

#include <QWidget>
// ##
#include <QScxmlStateMachine>
#include "mqttform.h"
#include "string"

namespace Ui {
class DeviceForm;
}

// ##
//class DeviceForm : public QWidget
class DeviceForm : public MqttForm
{
    Q_OBJECT

public:
    explicit DeviceForm(QWidget *parent = 0);
    ~DeviceForm();

private slots:
    void on_buttonpub_clicked();
    void on_greenon_clicked();
    void on_yellowon_clicked();
    void on_redon_clicked();
    void on_greenoff_clicked();
    void on_yellowoff_clicked();
    void on_redoff_clicked();

private:
    Ui::DeviceForm *ui;
    // ##
    QScxmlStateMachine *stateMachine;
    int count_{0};
    int toggleLed_{0};
    char green[1] = {'1'};
    char yellow[1] = {'1'};
    char red[1] = {'1'};

};

#endif // DEVICEFORM_H

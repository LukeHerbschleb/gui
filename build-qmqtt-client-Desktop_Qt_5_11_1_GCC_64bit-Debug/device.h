﻿//
// Statemachine code from reading SCXML file 'device.scxml'
// Created by: The Qt SCXML Compiler version 1 (Qt 5.11.1)
// WARNING! All changes made in this file will be lost!
//

#ifndef DEVICE_H
#define DEVICE_H

#include <QScxmlStateMachine>
#include <QString>
#include <QVariant>

class device: public QScxmlStateMachine
{
    /* qmake ignore Q_OBJECT */
    Q_OBJECT
    Q_PROPERTY(bool S_Counting)
    Q_PROPERTY(bool S_Resetting)


public:
    Q_INVOKABLE device(QObject *parent = 0);
    ~device();



Q_SIGNALS:


private:
    struct Data;
    friend struct Data;
    struct Data *data;
};

Q_DECLARE_METATYPE(::device*)

#endif // DEVICE_H

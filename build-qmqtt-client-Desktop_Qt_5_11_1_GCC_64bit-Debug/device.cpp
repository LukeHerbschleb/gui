﻿//
// Statemachine code from reading SCXML file 'device.scxml'
// Created by: The Qt SCXML Compiler version 1 (Qt 5.11.1)
// WARNING! All changes made in this file will be lost!
//

#include "device.h"

#include <qscxmlinvokableservice.h>
#include <qscxmltabledata.h>
#include <QScxmlNullDataModel>

#if !defined(Q_QSCXMLC_OUTPUT_REVISION)
#error "The header file 'device.scxml' doesn't include <qscxmltabledata.h>."
#elif Q_QSCXMLC_OUTPUT_REVISION != 1
#error "This file was generated using the qscxmlc from 5.11.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The qscxmlc has changed too much.)"
#endif

struct device::Data: private QScxmlTableData {
    Data(device &stateMachine)
        : stateMachine(stateMachine)
    {}

    void init() {
        stateMachine.setTableData(this);
        stateMachine.setDataModel(&dataModel);
    }

    QString name() const Q_DECL_OVERRIDE Q_DECL_FINAL
    { return string(0); }

    QScxmlExecutableContent::ContainerId initialSetup() const Q_DECL_OVERRIDE Q_DECL_FINAL
    { return -1; }

    QScxmlExecutableContent::InstructionId *instructions() const Q_DECL_OVERRIDE Q_DECL_FINAL
    { return theInstructions; }

    QScxmlExecutableContent::StringId *dataNames(int *count) const Q_DECL_OVERRIDE Q_DECL_FINAL
    { *count = 0; return dataIds; }

    QScxmlExecutableContent::EvaluatorInfo evaluatorInfo(QScxmlExecutableContent::EvaluatorId evaluatorId) const Q_DECL_OVERRIDE Q_DECL_FINAL
    { Q_ASSERT(evaluatorId >= 0); Q_ASSERT(evaluatorId < 0); return evaluators[evaluatorId]; }

    QScxmlExecutableContent::AssignmentInfo assignmentInfo(QScxmlExecutableContent::EvaluatorId assignmentId) const Q_DECL_OVERRIDE Q_DECL_FINAL
    { Q_ASSERT(assignmentId >= 0); Q_ASSERT(assignmentId < 0); return assignments[assignmentId]; }

    QScxmlExecutableContent::ForeachInfo foreachInfo(QScxmlExecutableContent::EvaluatorId foreachId) const Q_DECL_OVERRIDE Q_DECL_FINAL
    { Q_ASSERT(foreachId >= 0); Q_ASSERT(foreachId < 0); return foreaches[foreachId]; }

    QString string(QScxmlExecutableContent::StringId id) const Q_DECL_OVERRIDE Q_DECL_FINAL
    {
        Q_ASSERT(id >= QScxmlExecutableContent::NoString); Q_ASSERT(id < 6);
        if (id == QScxmlExecutableContent::NoString) return QString();
        return QString({static_cast<QStringData*>(strings.data + id)});
    }

    const qint32 *stateMachineTable() const Q_DECL_OVERRIDE Q_DECL_FINAL
    { return theStateMachineTable; }

    QScxmlInvokableServiceFactory *serviceFactory(int id) const Q_DECL_OVERRIDE Q_DECL_FINAL;

    device &stateMachine;
    QScxmlNullDataModel dataModel;

    static QScxmlExecutableContent::ParameterInfo param(QScxmlExecutableContent::StringId name,
                                                        QScxmlExecutableContent::EvaluatorId expr,
                                                        QScxmlExecutableContent::StringId location)
    {
        QScxmlExecutableContent::ParameterInfo p;
        p.name = name;
        p.expr = expr;
        p.location = location;
        return p;
    }

    static QScxmlExecutableContent::InvokeInfo invoke(
            QScxmlExecutableContent::StringId id,
            QScxmlExecutableContent::StringId prefix,
            QScxmlExecutableContent::EvaluatorId expr,
            QScxmlExecutableContent::StringId location,
            QScxmlExecutableContent::StringId context,
            QScxmlExecutableContent::ContainerId finalize,
            bool autoforward)
    {
        QScxmlExecutableContent::InvokeInfo i;
        i.id = id;
        i.prefix = prefix;
        i.expr = expr;
        i.location = location;
        i.context = context;
        i.finalize = finalize;
        i.autoforward = autoforward;
        return i;
    }

    static qint32 theInstructions[];
    static QScxmlExecutableContent::StringId dataIds[];
    static QScxmlExecutableContent::EvaluatorInfo evaluators[];
    static QScxmlExecutableContent::AssignmentInfo assignments[];
    static QScxmlExecutableContent::ForeachInfo foreaches[];
    static const qint32 theStateMachineTable[];
    static struct Strings {
        QArrayData data[6];
        qunicodechar stringdata[63];
    } strings;
};

device::device(QObject *parent)
    : QScxmlStateMachine(&staticMetaObject, parent)
    , data(new Data(*this))
{ qRegisterMetaType<device *>(); data->init(); }

device::~device()
{ delete data; }

QScxmlInvokableServiceFactory *device::Data::serviceFactory(int id) const
{
    Q_UNUSED(id);
    Q_UNREACHABLE();
}

qint32 device::Data::theInstructions[] = {
2, 1, 2, 1, 0
};

QScxmlExecutableContent::StringId device::Data::dataIds[] = {
-1
};

QScxmlExecutableContent::EvaluatorInfo device::Data::evaluators[] = {
{ -1, -1 }
};

QScxmlExecutableContent::AssignmentInfo device::Data::assignments[] = {
{ -1, -1, -1 }
};

QScxmlExecutableContent::ForeachInfo device::Data::foreaches[] = {
{ -1, -1, -1, -1 }
};

#define STR_LIT(idx, ofs, len) \
    Q_STATIC_STRING_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(Strings, stringdata) + ofs * sizeof(qunicodechar) - idx * sizeof(QArrayData)) \
    )
device::Data::Strings device::Data::strings = {{
STR_LIT(0, 0, 6), STR_LIT(1, 7, 10), STR_LIT(2, 18, 12), STR_LIT(3, 31, 7),
STR_LIT(4, 39, 11), STR_LIT(5, 51, 10)
},{
0x64,0x65,0x76,0x69,0x63,0x65,0, // 0: device
0x53,0x5f,0x43,0x6f,0x75,0x6e,0x74,0x69,0x6e,0x67,0, // 1: S_Counting
0x45,0x5f,0x4d,0x61,0x78,0x52,0x65,0x61,0x63,0x68,0x65,0x64,0, // 2: E_MaxReached
0x45,0x5f,0x43,0x6f,0x75,0x6e,0x74,0, // 3: E_Count
0x53,0x5f,0x52,0x65,0x73,0x65,0x74,0x74,0x69,0x6e,0x67,0, // 4: S_Resetting
0x45,0x5f,0x43,0x6f,0x6e,0x74,0x69,0x6e,0x75,0x65,0 // 5: E_Continue
}};

const qint32 device::Data::theStateMachineTable[] = {
	0x1, // version
	0, // name
	0, // data-model
	17, // child states array offset
	3, // transition to initial states
	-1, // initial setup
	0, // binding
	-1, // maxServiceId
	14, 2, // state offset and count
	36, 4, // transition offset and count
	60, 22, // array offset and size

	// States:
	1, -1, 0, -1, -1, 0, -1, -1, -1, 8, -1,
	4, -1, 0, -1, -1, -1, -1, -1, -1, 15, -1,

	// Transitions:
	2, -1, 1, 0, 0, -1, 
	6, -1, 1, 0, 4, -1, 
	13, -1, 1, 1, 11, -1, 
	-1, -1, 2, -1, 20, -1, 

	// Arrays:
	1, 1, 
	1, 2, 
	1, 0, 
	1, 3, 
	2, 0, 1, 
	1, 0, 
	1, 5, 
	1, 2, 
	2, 0, 1, 
	1, 0, 

	0xc0ff33 // terminator
};

struct qt_meta_stringdata_device_t {
    QByteArrayData data[8];
    unsigned char stringdata0[82];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_device_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_device_t qt_meta_stringdata_device = {
    {
QT_MOC_LITERAL(0, 0, 6), // "device"
QT_MOC_LITERAL(1, 7, 17), // "S_CountingChanged"
QT_MOC_LITERAL(2, 25, 0), // ""
QT_MOC_LITERAL(3, 26, 6), // "active"
QT_MOC_LITERAL(4, 33, 18), // "S_ResettingChanged"
QT_MOC_LITERAL(5, 52, 6), // "parent"
QT_MOC_LITERAL(6, 59, 10), // "S_Counting"
QT_MOC_LITERAL(7, 70, 11) // "S_Resetting"
    },{
0x64,0x65,0x76,0x69,0x63,0x65,0, // 0: device
0x53,0x5f,0x43,0x6f,0x75,0x6e,0x74,0x69,0x6e,0x67,0x43,0x68,0x61,0x6e,0x67,0x65,0x64,0, // 1: S_CountingChanged
0, // 2: 
0x61,0x63,0x74,0x69,0x76,0x65,0, // 3: active
0x53,0x5f,0x52,0x65,0x73,0x65,0x74,0x74,0x69,0x6e,0x67,0x43,0x68,0x61,0x6e,0x67,0x65,0x64,0, // 4: S_ResettingChanged
0x70,0x61,0x72,0x65,0x6e,0x74,0, // 5: parent
0x53,0x5f,0x43,0x6f,0x75,0x6e,0x74,0x69,0x6e,0x67,0, // 6: S_Counting
0x53,0x5f,0x52,0x65,0x73,0x65,0x74,0x74,0x69,0x6e,0x67,0 // 7: S_Resetting
    }};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_device[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       2,   14, // methods
       2,   33, // properties
       0,    0, // enums/sets
       1,   41, // constructors
       0,       // flags
       2,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,   24,    2, 0x06 /* Public */,
       4,    1,   27,    2, 0x06 /* Public */,

 // signals: parameters
    QMetaType::Void, QMetaType::Bool,    3,
    QMetaType::Void, QMetaType::Bool,    3,

 // constructors: parameters
    0x80000000 | 2, QMetaType::QObjectStar,    5,

 // properties: name, type, flags
       6, QMetaType::Bool, 0x006a6001,
       7, QMetaType::Bool, 0x006a6001,

 // properties: notify_signal_id
       0,
       1,

 // constructors: name, argc, parameters, tag, flags
       0,    1,   30,    2, 0x0e /* Public */,

       0        // eod
};

void device::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::CreateInstance) {
        switch (_id) {
        case 0: { device *_r = new device((*reinterpret_cast< QObject **>(_a[1])));
            if (_a[0]) *reinterpret_cast<QObject**>(_a[0]) = _r; } break;
        default: break;
        }
    } else if (_c == QMetaObject::InvokeMetaMethod) {
        device *_t = static_cast<device *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: QMetaObject::activate(_o, &staticMetaObject, 0, _a); break;
        case 1: QMetaObject::activate(_o, &staticMetaObject, 1, _a); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        Q_UNUSED(result);
        Q_UNUSED(func);
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        device *_t = static_cast<device *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< bool*>(_v) = _t->isActive(0); break;
        case 1: *reinterpret_cast< bool*>(_v) = _t->isActive(1); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
}

const QMetaObject device::staticMetaObject = {
    { &QScxmlStateMachine::staticMetaObject, qt_meta_stringdata_device.data,
      qt_meta_data_device,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *device::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *device::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, reinterpret_cast<const char *>(
            qt_meta_stringdata_device.stringdata0)))
        return static_cast<void*>(const_cast< device*>(this));
    return QScxmlStateMachine::qt_metacast(_clname);
}

int device::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QScxmlStateMachine::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 2)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 2;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 2)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 2;
    }
#ifndef QT_NO_PROPERTIES
   else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 2;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 2;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 2;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 2;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 2;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 2;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}



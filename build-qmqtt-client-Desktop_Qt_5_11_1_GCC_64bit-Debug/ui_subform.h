/********************************************************************************
** Form generated from reading UI file 'subform.ui'
**
** Created by: Qt User Interface Compiler version 5.11.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SUBFORM_H
#define UI_SUBFORM_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_SubForm
{
public:
    QHBoxLayout *horizontalLayout;
    QHBoxLayout *horizontalLayout_2;
    QGroupBox *groupBox;
    QVBoxLayout *verticalLayout_2;
    QGridLayout *gridLayout;
    QLabel *topicLabel;
    QComboBox *qosComboBox;
    QLabel *qosLabel;
    QLineEdit *topicLineEdit;
    QLabel *label;
    QListWidget *listWidget;
    QVBoxLayout *verticalLayout;
    QPushButton *subButton;
    QPushButton *unsubButton;
    QSpacerItem *verticalSpacer;

    void setupUi(QWidget *SubForm)
    {
        if (SubForm->objectName().isEmpty())
            SubForm->setObjectName(QStringLiteral("SubForm"));
        SubForm->resize(540, 235);
        horizontalLayout = new QHBoxLayout(SubForm);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        horizontalLayout->setContentsMargins(0, -1, -1, -1);
        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        groupBox = new QGroupBox(SubForm);
        groupBox->setObjectName(QStringLiteral("groupBox"));
        verticalLayout_2 = new QVBoxLayout(groupBox);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        gridLayout = new QGridLayout();
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        topicLabel = new QLabel(groupBox);
        topicLabel->setObjectName(QStringLiteral("topicLabel"));

        gridLayout->addWidget(topicLabel, 0, 0, 1, 1);

        qosComboBox = new QComboBox(groupBox);
        qosComboBox->addItem(QString());
        qosComboBox->addItem(QString());
        qosComboBox->addItem(QString());
        qosComboBox->setObjectName(QStringLiteral("qosComboBox"));

        gridLayout->addWidget(qosComboBox, 0, 3, 1, 1);

        qosLabel = new QLabel(groupBox);
        qosLabel->setObjectName(QStringLiteral("qosLabel"));

        gridLayout->addWidget(qosLabel, 0, 2, 1, 1);

        topicLineEdit = new QLineEdit(groupBox);
        topicLineEdit->setObjectName(QStringLiteral("topicLineEdit"));

        gridLayout->addWidget(topicLineEdit, 0, 1, 1, 1);


        verticalLayout_2->addLayout(gridLayout);

        label = new QLabel(groupBox);
        label->setObjectName(QStringLiteral("label"));

        verticalLayout_2->addWidget(label);

        listWidget = new QListWidget(groupBox);
        listWidget->setObjectName(QStringLiteral("listWidget"));

        verticalLayout_2->addWidget(listWidget);


        horizontalLayout_2->addWidget(groupBox);

        horizontalLayout_2->setStretch(0, 1);

        horizontalLayout->addLayout(horizontalLayout_2);

        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        subButton = new QPushButton(SubForm);
        subButton->setObjectName(QStringLiteral("subButton"));

        verticalLayout->addWidget(subButton);

        unsubButton = new QPushButton(SubForm);
        unsubButton->setObjectName(QStringLiteral("unsubButton"));

        verticalLayout->addWidget(unsubButton);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer);


        horizontalLayout->addLayout(verticalLayout);

#ifndef QT_NO_SHORTCUT
        topicLabel->setBuddy(topicLineEdit);
        qosLabel->setBuddy(qosComboBox);
#endif // QT_NO_SHORTCUT
        QWidget::setTabOrder(topicLineEdit, qosComboBox);
        QWidget::setTabOrder(qosComboBox, subButton);
        QWidget::setTabOrder(subButton, unsubButton);
        QWidget::setTabOrder(unsubButton, listWidget);

        retranslateUi(SubForm);
        QObject::connect(topicLineEdit, SIGNAL(textChanged(QString)), SubForm, SLOT(onTopicInput(QString)));
        QObject::connect(subButton, SIGNAL(clicked()), SubForm, SLOT(onSubscribe()));
        QObject::connect(unsubButton, SIGNAL(clicked()), SubForm, SLOT(onUnsubscribe()));

        QMetaObject::connectSlotsByName(SubForm);
    } // setupUi

    void retranslateUi(QWidget *SubForm)
    {
        SubForm->setWindowTitle(QApplication::translate("SubForm", "Form", nullptr));
        groupBox->setTitle(QString());
        topicLabel->setText(QApplication::translate("SubForm", "Topic:", nullptr));
        qosComboBox->setItemText(0, QApplication::translate("SubForm", "QOS0", nullptr));
        qosComboBox->setItemText(1, QApplication::translate("SubForm", "QOS1", nullptr));
        qosComboBox->setItemText(2, QApplication::translate("SubForm", "QOS2", nullptr));

        qosLabel->setText(QApplication::translate("SubForm", "Qos:", nullptr));
        label->setText(QApplication::translate("SubForm", "Subscribed Topics:", nullptr));
        subButton->setText(QApplication::translate("SubForm", "&Subscribe", nullptr));
        unsubButton->setText(QApplication::translate("SubForm", "U&nsubscribe", nullptr));
    } // retranslateUi

};

namespace Ui {
    class SubForm: public Ui_SubForm {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SUBFORM_H

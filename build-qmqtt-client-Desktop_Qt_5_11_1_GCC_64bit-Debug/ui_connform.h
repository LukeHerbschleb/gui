/********************************************************************************
** Form generated from reading UI file 'connform.ui'
**
** Created by: Qt User Interface Compiler version 5.11.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_CONNFORM_H
#define UI_CONNFORM_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QFrame>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_ConnForm
{
public:
    QHBoxLayout *horizontalLayout;
    QGroupBox *groupBox;
    QGridLayout *gridLayout;
    QLabel *label_8;
    QLabel *label;
    QLabel *label_9;
    QLabel *label_2;
    QFrame *line;
    QLineEdit *leUser;
    QLineEdit *leWillTopic;
    QLineEdit *leHost;
    QLabel *label_7;
    QLabel *label_6;
    QSpinBox *sbPort;
    QLineEdit *lePasswd;
    QLabel *label_5;
    QTextEdit *teWillMsg;
    QLineEdit *leClientId;
    QLabel *label_3;
    QSpinBox *sbKeepalive;
    QLabel *label_4;
    QCheckBox *cbCleanSess;
    QVBoxLayout *verticalLayout;
    QPushButton *connButton;
    QPushButton *disconnButton;
    QSpacerItem *verticalSpacer;

    void setupUi(QWidget *ConnForm)
    {
        if (ConnForm->objectName().isEmpty())
            ConnForm->setObjectName(QStringLiteral("ConnForm"));
        ConnForm->resize(562, 309);
        ConnForm->setMaximumSize(QSize(16777215, 16777215));
        horizontalLayout = new QHBoxLayout(ConnForm);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        horizontalLayout->setContentsMargins(0, -1, -1, -1);
        groupBox = new QGroupBox(ConnForm);
        groupBox->setObjectName(QStringLiteral("groupBox"));
        gridLayout = new QGridLayout(groupBox);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        label_8 = new QLabel(groupBox);
        label_8->setObjectName(QStringLiteral("label_8"));

        gridLayout->addWidget(label_8, 5, 2, 1, 1);

        label = new QLabel(groupBox);
        label->setObjectName(QStringLiteral("label"));

        gridLayout->addWidget(label, 7, 0, 1, 1);

        label_9 = new QLabel(groupBox);
        label_9->setObjectName(QStringLiteral("label_9"));

        gridLayout->addWidget(label_9, 2, 0, 1, 1);

        label_2 = new QLabel(groupBox);
        label_2->setObjectName(QStringLiteral("label_2"));

        gridLayout->addWidget(label_2, 8, 0, 1, 1);

        line = new QFrame(groupBox);
        line->setObjectName(QStringLiteral("line"));
        line->setFrameShape(QFrame::HLine);
        line->setFrameShadow(QFrame::Sunken);

        gridLayout->addWidget(line, 3, 0, 1, 4);

        leUser = new QLineEdit(groupBox);
        leUser->setObjectName(QStringLiteral("leUser"));

        gridLayout->addWidget(leUser, 5, 1, 1, 1);

        leWillTopic = new QLineEdit(groupBox);
        leWillTopic->setObjectName(QStringLiteral("leWillTopic"));

        gridLayout->addWidget(leWillTopic, 7, 1, 1, 3);

        leHost = new QLineEdit(groupBox);
        leHost->setObjectName(QStringLiteral("leHost"));

        gridLayout->addWidget(leHost, 0, 1, 1, 1);

        label_7 = new QLabel(groupBox);
        label_7->setObjectName(QStringLiteral("label_7"));

        gridLayout->addWidget(label_7, 5, 0, 1, 1);

        label_6 = new QLabel(groupBox);
        label_6->setObjectName(QStringLiteral("label_6"));

        gridLayout->addWidget(label_6, 0, 0, 1, 1);

        sbPort = new QSpinBox(groupBox);
        sbPort->setObjectName(QStringLiteral("sbPort"));
        sbPort->setMinimum(1);
        sbPort->setMaximum(65535);
        sbPort->setValue(1883);

        gridLayout->addWidget(sbPort, 0, 3, 1, 1);

        lePasswd = new QLineEdit(groupBox);
        lePasswd->setObjectName(QStringLiteral("lePasswd"));

        gridLayout->addWidget(lePasswd, 5, 3, 1, 1);

        label_5 = new QLabel(groupBox);
        label_5->setObjectName(QStringLiteral("label_5"));

        gridLayout->addWidget(label_5, 0, 2, 1, 1);

        teWillMsg = new QTextEdit(groupBox);
        teWillMsg->setObjectName(QStringLiteral("teWillMsg"));

        gridLayout->addWidget(teWillMsg, 8, 1, 1, 3);

        leClientId = new QLineEdit(groupBox);
        leClientId->setObjectName(QStringLiteral("leClientId"));

        gridLayout->addWidget(leClientId, 2, 1, 1, 3);

        label_3 = new QLabel(groupBox);
        label_3->setObjectName(QStringLiteral("label_3"));

        gridLayout->addWidget(label_3, 1, 0, 1, 1);

        sbKeepalive = new QSpinBox(groupBox);
        sbKeepalive->setObjectName(QStringLiteral("sbKeepalive"));
        sbKeepalive->setSuffix(QStringLiteral("(s)"));
        sbKeepalive->setMaximum(3600);
        sbKeepalive->setValue(300);

        gridLayout->addWidget(sbKeepalive, 1, 1, 1, 1);

        label_4 = new QLabel(groupBox);
        label_4->setObjectName(QStringLiteral("label_4"));

        gridLayout->addWidget(label_4, 1, 2, 1, 1);

        cbCleanSess = new QCheckBox(groupBox);
        cbCleanSess->setObjectName(QStringLiteral("cbCleanSess"));
        cbCleanSess->setChecked(true);

        gridLayout->addWidget(cbCleanSess, 1, 3, 1, 1);


        horizontalLayout->addWidget(groupBox);

        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        connButton = new QPushButton(ConnForm);
        connButton->setObjectName(QStringLiteral("connButton"));

        verticalLayout->addWidget(connButton);

        disconnButton = new QPushButton(ConnForm);
        disconnButton->setObjectName(QStringLiteral("disconnButton"));

        verticalLayout->addWidget(disconnButton);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer);


        horizontalLayout->addLayout(verticalLayout);

#ifndef QT_NO_SHORTCUT
        label_8->setBuddy(lePasswd);
        label->setBuddy(leWillTopic);
        label_9->setBuddy(leClientId);
        label_2->setBuddy(teWillMsg);
        label_7->setBuddy(leUser);
        label_6->setBuddy(leHost);
        label_5->setBuddy(sbPort);
        label_3->setBuddy(sbKeepalive);
        label_4->setBuddy(cbCleanSess);
#endif // QT_NO_SHORTCUT
        QWidget::setTabOrder(leHost, sbPort);
        QWidget::setTabOrder(sbPort, sbKeepalive);
        QWidget::setTabOrder(sbKeepalive, cbCleanSess);
        QWidget::setTabOrder(cbCleanSess, leClientId);
        QWidget::setTabOrder(leClientId, leUser);
        QWidget::setTabOrder(leUser, lePasswd);
        QWidget::setTabOrder(lePasswd, leWillTopic);
        QWidget::setTabOrder(leWillTopic, teWillMsg);
        QWidget::setTabOrder(teWillMsg, connButton);
        QWidget::setTabOrder(connButton, disconnButton);

        retranslateUi(ConnForm);
        QObject::connect(connButton, SIGNAL(clicked()), ConnForm, SLOT(onConnect()));
        QObject::connect(disconnButton, SIGNAL(clicked()), ConnForm, SLOT(onDisconnect()));

        QMetaObject::connectSlotsByName(ConnForm);
    } // setupUi

    void retranslateUi(QWidget *ConnForm)
    {
        ConnForm->setWindowTitle(QApplication::translate("ConnForm", "Form", nullptr));
        groupBox->setTitle(QString());
        label_8->setText(QApplication::translate("ConnForm", "Password:", nullptr));
        label->setText(QApplication::translate("ConnForm", "Will Topic:", nullptr));
        label_9->setText(QApplication::translate("ConnForm", "ClientId:", nullptr));
        label_2->setText(QApplication::translate("ConnForm", "Will Msg:", nullptr));
        leHost->setText(QApplication::translate("ConnForm", "127.0.0.1", nullptr));
        label_7->setText(QApplication::translate("ConnForm", "Username:", nullptr));
        label_6->setText(QApplication::translate("ConnForm", "Server(*):", nullptr));
        label_5->setText(QApplication::translate("ConnForm", "Port(*):", nullptr));
        leClientId->setText(QApplication::translate("ConnForm", "SomeMQTTid", nullptr));
        label_3->setText(QApplication::translate("ConnForm", "Keepalive:", nullptr));
        label_4->setText(QApplication::translate("ConnForm", "CleanSess:", nullptr));
        cbCleanSess->setText(QString());
        connButton->setText(QApplication::translate("ConnForm", "Connect", nullptr));
        disconnButton->setText(QApplication::translate("ConnForm", "Disconnect", nullptr));
    } // retranslateUi

};

namespace Ui {
    class ConnForm: public Ui_ConnForm {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_CONNFORM_H

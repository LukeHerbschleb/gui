/********************************************************************************
** Form generated from reading UI file 'pubform.ui'
**
** Created by: Qt User Interface Compiler version 5.11.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_PUBFORM_H
#define UI_PUBFORM_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_PubForm
{
public:
    QHBoxLayout *horizontalLayout;
    QGroupBox *groupBox;
    QGridLayout *gridLayout;
    QComboBox *qOSComboBox;
    QLabel *topicLabel;
    QLabel *messageLabel;
    QLabel *qOSLabel;
    QTextEdit *payloadEdit;
    QLineEdit *topicLineEdit;
    QLabel *retainLabel;
    QCheckBox *retainCheckBox;
    QVBoxLayout *verticalLayout;
    QPushButton *pubButton;
    QSpacerItem *verticalSpacer;

    void setupUi(QWidget *PubForm)
    {
        if (PubForm->objectName().isEmpty())
            PubForm->setObjectName(QStringLiteral("PubForm"));
        PubForm->resize(540, 222);
        horizontalLayout = new QHBoxLayout(PubForm);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        horizontalLayout->setContentsMargins(0, -1, -1, -1);
        groupBox = new QGroupBox(PubForm);
        groupBox->setObjectName(QStringLiteral("groupBox"));
        gridLayout = new QGridLayout(groupBox);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        qOSComboBox = new QComboBox(groupBox);
        qOSComboBox->addItem(QString());
        qOSComboBox->addItem(QString());
        qOSComboBox->addItem(QString());
        qOSComboBox->setObjectName(QStringLiteral("qOSComboBox"));

        gridLayout->addWidget(qOSComboBox, 1, 2, 1, 1);

        topicLabel = new QLabel(groupBox);
        topicLabel->setObjectName(QStringLiteral("topicLabel"));

        gridLayout->addWidget(topicLabel, 0, 0, 1, 1);

        messageLabel = new QLabel(groupBox);
        messageLabel->setObjectName(QStringLiteral("messageLabel"));

        gridLayout->addWidget(messageLabel, 6, 0, 1, 1);

        qOSLabel = new QLabel(groupBox);
        qOSLabel->setObjectName(QStringLiteral("qOSLabel"));

        gridLayout->addWidget(qOSLabel, 1, 0, 1, 1);

        payloadEdit = new QTextEdit(groupBox);
        payloadEdit->setObjectName(QStringLiteral("payloadEdit"));

        gridLayout->addWidget(payloadEdit, 6, 2, 1, 5);

        topicLineEdit = new QLineEdit(groupBox);
        topicLineEdit->setObjectName(QStringLiteral("topicLineEdit"));

        gridLayout->addWidget(topicLineEdit, 0, 2, 1, 5);

        retainLabel = new QLabel(groupBox);
        retainLabel->setObjectName(QStringLiteral("retainLabel"));

        gridLayout->addWidget(retainLabel, 2, 0, 1, 1);

        retainCheckBox = new QCheckBox(groupBox);
        retainCheckBox->setObjectName(QStringLiteral("retainCheckBox"));

        gridLayout->addWidget(retainCheckBox, 2, 2, 1, 1);


        horizontalLayout->addWidget(groupBox);

        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        pubButton = new QPushButton(PubForm);
        pubButton->setObjectName(QStringLiteral("pubButton"));

        verticalLayout->addWidget(pubButton);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer);


        horizontalLayout->addLayout(verticalLayout);

        horizontalLayout->setStretch(0, 1);
        QWidget::setTabOrder(topicLineEdit, qOSComboBox);
        QWidget::setTabOrder(qOSComboBox, retainCheckBox);
        QWidget::setTabOrder(retainCheckBox, payloadEdit);
        QWidget::setTabOrder(payloadEdit, pubButton);

        retranslateUi(PubForm);
        QObject::connect(pubButton, SIGNAL(clicked()), PubForm, SLOT(onPublish()));
        QObject::connect(topicLineEdit, SIGNAL(textEdited(QString)), PubForm, SLOT(onPublishReady()));
        QObject::connect(payloadEdit, SIGNAL(textChanged()), PubForm, SLOT(onPublishReady()));

        QMetaObject::connectSlotsByName(PubForm);
    } // setupUi

    void retranslateUi(QWidget *PubForm)
    {
        PubForm->setWindowTitle(QApplication::translate("PubForm", "Form", nullptr));
        groupBox->setTitle(QString());
        qOSComboBox->setItemText(0, QApplication::translate("PubForm", "QOS0", nullptr));
        qOSComboBox->setItemText(1, QApplication::translate("PubForm", "QOS1", nullptr));
        qOSComboBox->setItemText(2, QApplication::translate("PubForm", "QOS2", nullptr));

        topicLabel->setText(QApplication::translate("PubForm", "Topic(*):", nullptr));
        messageLabel->setText(QApplication::translate("PubForm", "Message(*):", nullptr));
        qOSLabel->setText(QApplication::translate("PubForm", "QOS:", nullptr));
        topicLineEdit->setText(QApplication::translate("PubForm", "ESEiot/1718/", nullptr));
        retainLabel->setText(QApplication::translate("PubForm", "Retain:", nullptr));
        pubButton->setText(QApplication::translate("PubForm", "&Publish", nullptr));
    } // retranslateUi

};

namespace Ui {
    class PubForm: public Ui_PubForm {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_PUBFORM_H

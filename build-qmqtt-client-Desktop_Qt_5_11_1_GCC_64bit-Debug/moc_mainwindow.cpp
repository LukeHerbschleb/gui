/****************************************************************************
** Meta object code from reading C++ file 'mainwindow.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.11.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../qmqtt-client-master/mainwindow.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'mainwindow.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.11.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_MainWindow_t {
    QByteArrayData data[33];
    char stringdata0[442];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_MainWindow_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_MainWindow_t qt_meta_stringdata_MainWindow = {
    {
QT_MOC_LITERAL(0, 0, 10), // "MainWindow"
QT_MOC_LITERAL(1, 11, 16), // "onMQTT_Connected"
QT_MOC_LITERAL(2, 28, 0), // ""
QT_MOC_LITERAL(3, 29, 16), // "onMQTT_Connacked"
QT_MOC_LITERAL(4, 46, 3), // "ack"
QT_MOC_LITERAL(5, 50, 12), // "onMQTT_error"
QT_MOC_LITERAL(6, 63, 18), // "QMQTT::ClientError"
QT_MOC_LITERAL(7, 82, 3), // "err"
QT_MOC_LITERAL(8, 86, 16), // "onMQTT_Published"
QT_MOC_LITERAL(9, 103, 14), // "QMQTT::Message"
QT_MOC_LITERAL(10, 118, 7), // "message"
QT_MOC_LITERAL(11, 126, 15), // "onMQTT_Pubacked"
QT_MOC_LITERAL(12, 142, 4), // "type"
QT_MOC_LITERAL(13, 147, 5), // "msgid"
QT_MOC_LITERAL(14, 153, 15), // "onMQTT_Received"
QT_MOC_LITERAL(15, 169, 17), // "onMQTT_subscribed"
QT_MOC_LITERAL(16, 187, 5), // "topic"
QT_MOC_LITERAL(17, 193, 15), // "onMQTT_subacked"
QT_MOC_LITERAL(18, 209, 3), // "qos"
QT_MOC_LITERAL(19, 213, 19), // "onMQTT_unsubscribed"
QT_MOC_LITERAL(20, 233, 17), // "onMQTT_unsubacked"
QT_MOC_LITERAL(21, 251, 11), // "onMQTT_Pong"
QT_MOC_LITERAL(22, 263, 19), // "onMQTT_disconnected"
QT_MOC_LITERAL(23, 283, 24), // "on_actionAbout_triggered"
QT_MOC_LITERAL(24, 308, 23), // "on_actionQuit_triggered"
QT_MOC_LITERAL(25, 332, 8), // "aboutApp"
QT_MOC_LITERAL(26, 341, 12), // "publishTopic"
QT_MOC_LITERAL(27, 354, 14), // "subscribeTopic"
QT_MOC_LITERAL(28, 369, 13), // "connectServer"
QT_MOC_LITERAL(29, 383, 7), // "quitApp"
QT_MOC_LITERAL(30, 391, 8), // "clearMsg"
QT_MOC_LITERAL(31, 400, 37), // "on_conForm_customContextMenuR..."
QT_MOC_LITERAL(32, 438, 3) // "pos"

    },
    "MainWindow\0onMQTT_Connected\0\0"
    "onMQTT_Connacked\0ack\0onMQTT_error\0"
    "QMQTT::ClientError\0err\0onMQTT_Published\0"
    "QMQTT::Message\0message\0onMQTT_Pubacked\0"
    "type\0msgid\0onMQTT_Received\0onMQTT_subscribed\0"
    "topic\0onMQTT_subacked\0qos\0onMQTT_unsubscribed\0"
    "onMQTT_unsubacked\0onMQTT_Pong\0"
    "onMQTT_disconnected\0on_actionAbout_triggered\0"
    "on_actionQuit_triggered\0aboutApp\0"
    "publishTopic\0subscribeTopic\0connectServer\0"
    "quitApp\0clearMsg\0on_conForm_customContextMenuRequested\0"
    "pos"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_MainWindow[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      21,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,  119,    2, 0x08 /* Private */,
       3,    1,  120,    2, 0x08 /* Private */,
       5,    1,  123,    2, 0x08 /* Private */,
       8,    1,  126,    2, 0x08 /* Private */,
      11,    2,  129,    2, 0x08 /* Private */,
      14,    1,  134,    2, 0x08 /* Private */,
      15,    1,  137,    2, 0x08 /* Private */,
      17,    2,  140,    2, 0x08 /* Private */,
      19,    1,  145,    2, 0x08 /* Private */,
      20,    1,  148,    2, 0x08 /* Private */,
      21,    0,  151,    2, 0x08 /* Private */,
      22,    0,  152,    2, 0x08 /* Private */,
      23,    0,  153,    2, 0x08 /* Private */,
      24,    0,  154,    2, 0x08 /* Private */,
      25,    0,  155,    2, 0x08 /* Private */,
      26,    0,  156,    2, 0x08 /* Private */,
      27,    0,  157,    2, 0x08 /* Private */,
      28,    0,  158,    2, 0x08 /* Private */,
      29,    0,  159,    2, 0x08 /* Private */,
      30,    0,  160,    2, 0x08 /* Private */,
      31,    1,  161,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void, QMetaType::UChar,    4,
    QMetaType::Void, 0x80000000 | 6,    7,
    QMetaType::Void, 0x80000000 | 9,   10,
    QMetaType::Void, QMetaType::UChar, QMetaType::UShort,   12,   13,
    QMetaType::Void, 0x80000000 | 9,   10,
    QMetaType::Void, QMetaType::QString,   16,
    QMetaType::Void, QMetaType::UShort, QMetaType::UChar,   13,   18,
    QMetaType::Void, QMetaType::QString,   16,
    QMetaType::Void, QMetaType::UShort,   13,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QPoint,   32,

       0        // eod
};

void MainWindow::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        MainWindow *_t = static_cast<MainWindow *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->onMQTT_Connected(); break;
        case 1: _t->onMQTT_Connacked((*reinterpret_cast< quint8(*)>(_a[1]))); break;
        case 2: _t->onMQTT_error((*reinterpret_cast< QMQTT::ClientError(*)>(_a[1]))); break;
        case 3: _t->onMQTT_Published((*reinterpret_cast< const QMQTT::Message(*)>(_a[1]))); break;
        case 4: _t->onMQTT_Pubacked((*reinterpret_cast< quint8(*)>(_a[1])),(*reinterpret_cast< quint16(*)>(_a[2]))); break;
        case 5: _t->onMQTT_Received((*reinterpret_cast< const QMQTT::Message(*)>(_a[1]))); break;
        case 6: _t->onMQTT_subscribed((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 7: _t->onMQTT_subacked((*reinterpret_cast< quint16(*)>(_a[1])),(*reinterpret_cast< quint8(*)>(_a[2]))); break;
        case 8: _t->onMQTT_unsubscribed((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 9: _t->onMQTT_unsubacked((*reinterpret_cast< quint16(*)>(_a[1]))); break;
        case 10: _t->onMQTT_Pong(); break;
        case 11: _t->onMQTT_disconnected(); break;
        case 12: _t->on_actionAbout_triggered(); break;
        case 13: _t->on_actionQuit_triggered(); break;
        case 14: _t->aboutApp(); break;
        case 15: _t->publishTopic(); break;
        case 16: _t->subscribeTopic(); break;
        case 17: _t->connectServer(); break;
        case 18: _t->quitApp(); break;
        case 19: _t->clearMsg(); break;
        case 20: _t->on_conForm_customContextMenuRequested((*reinterpret_cast< const QPoint(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 2:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QMQTT::ClientError >(); break;
            }
            break;
        case 3:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QMQTT::Message >(); break;
            }
            break;
        case 5:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QMQTT::Message >(); break;
            }
            break;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject MainWindow::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_MainWindow.data,
      qt_meta_data_MainWindow,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *MainWindow::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *MainWindow::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_MainWindow.stringdata0))
        return static_cast<void*>(this);
    return QMainWindow::qt_metacast(_clname);
}

int MainWindow::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 21)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 21;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 21)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 21;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
